<?php

namespace Verify;

trait Rules
{

    /*
     *  是否必填
     */
    public function required($key)
    {
        if($this->_input[$key] === '') {
            $this->_errors[] = $this->_rules[$key]['name'] . '不可为空';
            return false;
        }
        return true;
    }

    /*
     * 最大长度
     */
    public function maxLen($key, $val)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $len = mb_strlen($this->_input[$key]);
        if($len > $val) {
            $this->_errors[] = $this->_rules[$key]['name'] . '最大长度为'.$val;
            return false;
        }
        return true;
    }

    /*
     * 最小长度
     */
    public function minLen($key, $val)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $len = mb_strlen($this->_input[$key]);
        if($len < $val) {
            $this->_errors[] = $this->_rules[$key]['name'] . '最小长度为'.$val;
            return false;
        }
        return true;
    }

    /*
     * 验证手机号
     */
    public function mobile($key)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $result = preg_match('/^1([0-9]{10})$/', $this->_input[$key]);
        if(! $result) {
            $this->_errors[] = '请填写正确格式的手机号';
            return false;
        }
        return true;
    }

    /*
     * 验证邮箱
     */
    public function email($key)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $result = filter_var($this->_input[$key], FILTER_VALIDATE_EMAIL);
        if(! $result) {
            $this->_errors[] = '请填写正确格式的邮箱';
            return false;
        }
        return true;
    }

    /*
     * 数字
     */
    public function number($key)
    {
        if(! is_numeric($this->_input[$key])) {
            $this->_errors[] = $this->_rules[$key]['name'] . '必须为数字';
            return false;
        }
        return true;
    }

    /*
     * 最小值
     */
    public function min($key, $val)
    {
        if($this->_input[$key] < $val) {
            $this->_errors[] = $this->_rules[$key]['name'] . '最小值为'.$val;
            return false;
        }
        return true;
    }

    /*
     * 最大值
     */
    public function max($key, $val)
    {
        if($this->_input[$key] > $val) {
            $this->_errors[] = $this->_rules[$key]['name'] . '最大值为'.$val;
            return false;
        }
        return true;
    }

    /*
     * 坐标对
     * @format lat,lng[纬度,经度]
     */
    public function coordinate($key)
    {
        $values = explode(',', $this->_input[$key]);
        $count = count($values);
        if($count != 2) {
            $this->_errors[] = '请填写正确的坐标对，格式为[纬度,经度]，可在坐标拾取系统中直接复制';
            return false;
        }

        if($values[0] > 90) {
            $this->_errors[] = '请填写正确的坐标对，格式为[纬度,经度]，可在坐标拾取系统中直接复制。（纬度是小于90的数字）';
            return false;
        }
        return true;
    }

    /*
     * 唯一
     */
    public function unique($key)
    {
        if($this->_table == '') {
            throw new CheckException('系统错误：数据表不存在', 500);
        }
        $result = model("u")->dataRow($this->_table, array($key => $this->_input[$key]));
        if($result) {
            $this->_errors[] = $this->_rules[$key]['name'] . '已存在，请勿重复添加';
            return false;
        }
        return true;
    }

    /*
     * 唯一例外
     */
    public function uniqueEx($key, $val)
    {
        if($this->_table == '') {
            throw new CheckException('系统错误：数据表不存在', 500);
        }
        $result = model("u")->dataRow($this->_table, array($key => $this->_input[$key]));
        $arr = explode(',', $val);
        if(! $arr) {
            throw new CheckException('系统错误：验证参数错误', 500);
        }
        if($result && (! in_array($result['id'], $arr))) {
            $this->_errors[] = $this->_rules[$key]['name'] . '已存在，请勿重复添加';
            return false;
        }
        return true;
    }

    /*
     * 约束值
     */
    public function eq($key, $val)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        if($val != $this->_input[$key]) {
            $this->_errors[] = $this->_rules[$key]['name'] . '不正确';
            return false;
        }
        return true;

    }

    /*
     * 范围取值
     */
    public function in($key, $val)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $vals = explode(',', $val);
        if(! $vals) {
            $this->_errors[] = '参数错误';
            return true;
        }
        if(! in_array($this->_input[$key], $vals)) {
            $this->_errors[] = $this->_rules[$key]['name'] . '错误';
            return false;
        }
        return true;
    }

    /*
     * 日期
     */
    public function date($key, $sep)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        if(date('Y' . $sep . 'm' . $sep . 'd', strtotime($this->_input[$key])) == $this->_input[$key]) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     *  姓名
     *  长度不小于1的汉字
     */
    public function name($key)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $str = $this->_input[$key];
        if(strpos($str,'·')){
            //将·去掉，看看剩下的是不是都是中文
            $str=str_replace("·", '', $str);
        }
        if(preg_match('/^[\x7f-\xff]+$/', $str)){
            return true; //全是中文
        }else{
            $this->_errors[] = $this->_rules[$key]['name'] . '格式错误';
            return false; //不全是中文
        }
    }
    
    /*
     *  身份证号码
     */
    public function identify($key)
    {
        if($this->_input[$key] === '') {
            return true;
        }
        $str = $this->_input[$key];
        $vCity = [
            '11', '12', '13', '14', '15', '21', '22',
            '23', '31', '32', '33', '34', '35', '36',
            '37', '41', '42', '43', '44', '45', '46',
            '50', '51', '52', '53', '54', '61', '62',
            '63', '64', '65', '71', '81', '82', '91'
        ];
        if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $str)) {
            $this->_errors[] = $this->_rules[$key]['name'] . '格式错误';
            return false;
        }
        if (! in_array(substr($str, 0, 2), $vCity)) {
            $this->_errors[] = $this->_rules[$key]['name'] . '格式错误';
            return false;
        }
        $str = preg_replace('/[xX]$/i', 'a', $str);
        $vLength = strlen($str);
        if ($vLength == 18) {
            $vBirthday = substr($str, 6, 4) . '-' . substr($str, 10, 2) . '-' . substr($str, 12, 2);
        } else {
            $vBirthday = '19' . substr($str, 6, 2) . '-' . substr($str, 8, 2) . '-' . substr($str, 10, 2);
        }
        if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) {
            $this->_errors[] = $this->_rules[$key]['name'] . '格式错误';
            return false;
        }
        if ($vLength == 18) {
            $vSum = 0;
            for ($i = 17 ; $i >= 0 ; $i--) {
                $vSubStr = substr($str, 17 - $i, 1);
                $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr , 11));
            }
            if($vSum % 11 != 1) {
                $this->_errors[] = $this->_rules[$key]['name'] . '格式错误';
                return false;
            }
        }
        return true;
    }
}